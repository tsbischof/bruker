import csv

import spectrum_analysis
import powder

def get_xyspectrum(filename):
    angles = list()
    counts = list()

    with open(filename) as stream_in:
        metadata = next(stream_in)

        reader = csv.reader(stream_in, delimiter=" ")

        for angle, intensity in reader:
            angles.append(float(angle))
            counts.append(float(intensity))

    return(spectrum_analysis.AngleSpectrum(angles, counts))
