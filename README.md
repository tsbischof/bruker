# bruker
Python interface to the Bruker XY format.

## Usage
	import bruker

	spectrum = bruker.get_xyspectrum(filename)
	print(spectrum.angles, spectrum.counts)

## Issues
There is a wiki and issue tracker for this project, through [bitbucket](http://bitbucket.org/tsbischof/bruker)
